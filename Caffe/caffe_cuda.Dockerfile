# Start with CUDA Caffe dependencies
FROM argusyk/caffe_cuda_deps:7.5
MAINTAINER Yaroslaw Dorogyy <cisco.rna@gmail.com>

# Move into Caffe repo
RUN cd /root/caffe && \
# Make and move into build directory
  mkdir build && cd build && \
# CMake
  cmake .. && \
# Make
  make -j"$(nproc)" all && \
  make install

#Jupyter install
RUN pip install notebook ipywidgets

# Add to Python path
ENV PYTHONPATH=/root/caffe/python:$PYTHONPATH

# Set ~/caffe as working directory
WORKDIR /root/caffe

#Jupyter cmd
CMD "/usr/local/bin/jupyter-notebook" "--ip="0.0.0.0"" "--no-browser"


