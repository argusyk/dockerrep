FROM argusyk/opencv_torch_cuda:7.5
MAINTAINER Yaroslaw Dorogyy <cisco.rna@gmail.com>

RUN sudo apt-get update && apt-get install -y libboost-all-dev

# Add NVIDIA Docker information 
LABEL com.nvidia.cudnn.version="5"

# Install cuDNN v5 
ENV ML_REPO_PKG=nvidia-machine-learning-repo-ubuntu1404_4.0-2_amd64.deb 
ENV CUDNN_REPO_PKG1=libcudnn5-dev_5.1.3-1+cuda7.5_amd64.deb
ENV CUDNN_REPO_PKG2=libcudnn5_5.1.3-1+cuda7.5_amd64.deb

RUN sudo apt-get install -y wget && \
    wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1404/x86_64/$ML_REPO_PKG && \
    dpkg -i $ML_REPO_PKG && \
    wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1404/x86_64/$CUDNN_REPO_PKG2 && \
    dpkg -i $CUDNN_REPO_PKG2 && \
    wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1404/x86_64/$CUDNN_REPO_PKG1 && \
    dpkg -i $CUDNN_REPO_PKG1

RUN cd ~ && \
    mkdir -p dlib-tmp && \
    cd dlib-tmp && \
    curl -L \
         https://github.com/davisking/dlib/archive/v19.2.tar.gz \
         -o dlib.tar.bz2 && \
    tar xf dlib.tar.bz2 && \
    cd dlib-19.2/python_examples && \
    mkdir build && \
    cd build && \
    cmake ../../tools/python && \
    cmake --build . --config Release && \
    cp dlib.so /usr/local/lib/python2.7/dist-packages && \
rm -rf ~/dlib-tmp

RUN cd ~/Projects && \
    mkdir Dlib && \
    cd Dlib && \
    git clone https://github.com/bikz05/ipython-notebooks.git

RUN sudo apt-get install -y libblas-dev liblapack-dev libatlas-base-dev gfortran
RUN pip2 install pillow scipy scikit-learn   

WORKDIR /root/Projects
