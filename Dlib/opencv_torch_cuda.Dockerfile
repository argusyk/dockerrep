FROM argusyk/torch_cuda:7.5
MAINTAINER Yaroslaw Dorogyy <cisco.rna@gmail.com>

RUN apt-get update && apt-get install -y python-dev \ 
    libxft-dev libfreetype6 libfreetype6-dev

RUN pip2 install matplotlib

RUN cd ~ && \
    mkdir -p ocv-tmp && \
    cd ocv-tmp && \
    curl -L https://github.com/Itseez/opencv/archive/2.4.13.zip -o ocv.zip && \
    unzip ocv.zip && \
    cd opencv-2.4.13 && \
    mkdir release && \
    cd release && \
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
          -D CMAKE_INSTALL_PREFIX=/usr/local \
          -D BUILD_PYTHON_SUPPORT=ON \
          -D DWITH_CUDA=ON \
          .. && \
    make -j8 && \
    make install && \
    rm -rf ~/ocv-tmp

RUN mkdir /root/Projects && \ 
    cd ~/Projects && \
    mkdir OpenCV && \
    cd OpenCV/ && \
    git clone https://github.com/strykeforce/vision.git && \
    cd ~/Projects && \
    mkdir Torch && \
    cd Torch && \
    mv ~/torch/tutorials . 

WORKDIR /root/Projects
