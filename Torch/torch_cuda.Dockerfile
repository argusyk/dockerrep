FROM argusyk/torch_cuda_deps:7.5 

# Run Torch7 installation scripts 
RUN cd /root/torch && \
  ./install.sh

# Export environment variables manually 
ENV LUA_PATH='/root/.luarocks/share/lua/5.1/?.lua;/root/.luarocks/share/lua/5.1/?/init.lua;/root/torch/install/share/lua/5.1/?.lua;/root/torch/install/share/lua/5.1/?/init.lua;./?.lua;/root/torch/install/share/luajit-2.1.0-beta1/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua' 
ENV LUA_CPATH='/root/.luarocks/lib/lua/5.1/?.so;/root/torch/install/lib/lua/5.1/?.so;./?.so;/usr/local/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so' 
ENV PATH=/root/torch/install/bin:$PATH 
ENV LD_LIBRARY_PATH=/root/torch/install/lib:$LD_LIBRARY_PATH 
ENV DYLD_LIBRARY_PATH=/root/torch/install/lib:$DYLD_LIBRARY_PATH 
ENV LUA_CPATH='/root/torch/install/lib/?.so;'$LUA_CPATH 

# Set ~/torch as working directory 
WORKDIR /root/torch

#Examples here
RUN git clone https://github.com/torch/tutorials.git

#Jupyter notebook
CMD "/usr/local/bin/jupyter-notebook" "--ip="0.0.0.0"" "--no-browser"


